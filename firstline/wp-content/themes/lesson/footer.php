<footer id="contact">
	<div class="small-container">
		<div id="map"></div>
	</div>
	<div class="contact">
		<div class="title">Контакты</div>
		<p>Адрес: Украина, г. Харьков, пер.1-й Лесопарковский, д.1</p>
		<a class="call" href="tel:+380998876555">+ 38 099-887-65-55</a>
		<a class="call" href="tel:+380998876555">+ 38 099-887-65-55</a>
		<a class="mail" href="mailto:mail@gmail.com">mail@gmail.com</a>
		<ul class="social">
			<li><a class="anim" href="#"><i class="anim fab fa-instagram"></i></a></li>
			<li><a class="anim" href="#"><i class="anim fab fa-facebook-f"></i></a></li>
		</ul>
		<div class="rubika"><a href="https://rubika.com.ua/">Создание сайта</a> <img alt="rubika" src="<?php bloginfo('template_directory') ?>/images/rubika.png"></div>
	</div>
</footer>	
</div>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHFfgCQnXbGOgf4tzRu6ox-yfeamoHZwo&callback=initMap"
 async defer></script>
<script>
  function initMap() {
   var element = document.getElementById('map');
   var options = {
   	disableDefaultUI: true,
    zoom: 15, 
    center: {lat: 50.035199, lng: 36.28047229999993}
   };

   var myMap = new google.maps.Map(element, options);

   var marker = new google.maps.Marker({
    position: {lat: 50.035199, lng: 36.28047229999993},
    map: myMap,
    icon: '<?php bloginfo('template_directory') ?>/images/map-marker.png'
   });

   var InfoWindow = new google.maps.InfoWindow({
    content: '<p><strong>Название</strong><br>Адрес: Украина, г. Харьков,  пер.1-й Лесопарковский, д.1<br></p>'
   });

   marker.addListener('click', function(){
    InfoWindow.open(myMap, marker);
   });
  }
 </script>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/select/jquery.formstyler.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/slick/slick.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/main.js"></script>
</body>
</html>