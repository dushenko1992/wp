<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
	<title>Index</title>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory') ?>/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory') ?>/slick/slick-theme.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/fontawesome/css/all.css">
	<link href="<?php bloginfo('template_directory') ?>/select/jquery.formstyler.css" rel="stylesheet" />
	<link href="<?php bloginfo('template_directory') ?>/select/jquery.formstyler.theme.css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory') ?>/css/style.css">
</head>
<body>
<div class="wrapper">
<header>
	<div class="header-container">
		<a class="logo pull-left" href="<?php bloginfo('template_directory') ?>/index.html">
			<img alt="logo" src="<?php bloginfo('template_directory') ?>/images/logo.jpg">
		</a>
		<ul class="phone pull-right">
			<li><a href="tel:+38066112233">+ 380 66-11-2233</a></li>
			<li><a href="tel:+38066112233">+ 380 66-11-2233</a></li>
		</ul>
		<nav class="pull-right">
			<div class="overlay-menu anim">
				<?php wp_nav_menu( array(
					'theme_location'  => '',
					'menu'            => '',
					'container'       => 'ul',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'menu',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
					'depth'           => 0,
					'walker'          => '',
				) ); ?>
			</div>
			<div class="logo-bars_container anim">
				<div class="logo-bars">
					<span class="anim"></span>
					<span class="anim"></span>
					<span class="anim"></span>
				</div>
			</div>
		</nav>
	</div>
</header>