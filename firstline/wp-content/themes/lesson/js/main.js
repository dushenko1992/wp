jQuery(document).ready(function($){
	$("nav a").click(function (e) {
		e.preventDefault();

		$("nav").toggleClass("opened");
		$('body,html').animate({scrollTop: $($(this).attr('href')).offset().top}, 1500);
	});

    $("header .logo-bars_container").click(function(){
        $("nav").toggleClass("opened");
    });


	var video = $(".video-container video").get(0),
		videoHeight	= ($(".video-container video").offset().top + $(".video-container video").outerHeight());

	
	$('.video-container video, .video-container .controls').click(function(){
		if(video.paused){
			video.play();
			$('.video-container').addClass("isplaying");
		}else{
			video.pause();
			$('.video-container').removeClass("isplaying");
		}
	});
	
	var timer = null;
	$(window).scroll(function(){
		clearTimeout(timer);

		timer = setTimeout(function(){
			if($(window).scrollTop() > videoHeight){
				video.pause();
				$('.video-container').removeClass("isplaying");
			}
		}, 50);
	});
});
