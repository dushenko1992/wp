<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'my');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':u<4dF2PO:6sVb5k OyZOOVj(CE^=Q_LdfOPGFQayy%>@Q?DG.^A8gR_u=`;U,dG');
define('SECURE_AUTH_KEY',  'sNitJmmL$-26V1Q?9)W@k_gcOiq)@-w5^VaGe$$.?72/w1i(mS7~wx1ng(b#g|2k');
define('LOGGED_IN_KEY',    'fV]b264ZW%E-.HBm,+KFHA&wG@).R{A|e_P-jy^;yjS1>/YY1<{%&lh3&qjEtwjH');
define('NONCE_KEY',        'ZE!M n5XlfyoE+$5j!JUMm^<a*(h8~mO-+u;$ ?P^4i{sfx)-8OOVKgA^ I(MAz#');
define('AUTH_SALT',        'G>fhO@HV{%?+>Kum,cJ*eOK+FS: 9+t={:rIfn|c@0*0w:DImWD}8:`o{s;)t`~I');
define('SECURE_AUTH_SALT', '[(7|3H?ZpK[%}-f]dwKQF gWlaGrt*29Z&z<04XXO^M3Q+hL(5Zt+6P^W!S% TGJ');
define('LOGGED_IN_SALT',   '#(+N|F#V%Do]{0/Z|ysGMU|o_tNviXwPMH<MXyVAOwShm7N}F-r/viS?l`0>S@!i');
define('NONCE_SALT',       'E4-U+RXUe/L@%+Rb9~AtY|scSQa_UDXvMwSZ$G,{O8+KMlN6/%: j0s@g5qr_L.W');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
